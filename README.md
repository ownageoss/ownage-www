# Ownage Website

## Overview

A [Material Design](https://material.io/) themed static website built using Hugo, deployed to gitlab pages and accessible at https://ownage.co.nz and https://www.ownage.co.nz.

## Setup

[Install Hugo](https://gohugo.io/getting-started/installing/).

Verify installation:

```sh
hugo version
```

Clone this repo and run the hugo server:

```sh
git clone https://gitlab.com/ownageoss/ownage-www.git
cd ownage-www
hugo server
```

## Deployment

Simply commit your changes and [gitlab-ci.yml](.gitlab-ci.yml) will automatically test and deploy your changes within a minute.